<!DOCTYPE html>
	<head>
		<title>Prompt Gator</title>
		<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
		<meta name="viewport" content="width=device-width" initial-scale="1">
		<meta name="author" content="Rachel Suarez">
		<link type="text/css" rel="stylesheet" href="css/promptGator.css">
	</head>
	<body>	
		<header></header>
		<section></section>
		<section></section>
		<footer></footer>
	</body>
</html>
