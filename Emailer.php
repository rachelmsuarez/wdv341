<?php
	class Emailer {
		/*
			The class wills store information required to send a php email.
			It will build, email, and use PHP mail().
			Variables needed: emailSubject, recipientAddress, senderAddress, emailMessage
		*/

		//Properties of the class	
	
		private $emailMessage; //body/content of the email
		private $emailSubject; 
		private $recipientAddress;
		private $senderAddress;	

		//Constructor function

		function __construct() {
			//fill this in later
		}

		//Mutators/Set Functions

		function setEmailMessage($inMessage) {
			$this->emailMessage = $inMessage;
		}

		function setEmailSubject($inSubject) {
			$this->emailSubject = $inSubject;
		}

		function setRecipientAddress($inRecipient) {
			$this->recipientAddress = $inRecipient;
		}

		function setSenderAddress($inSender) {
			$this->senderAddress = $inSender;
		}		

		//Mutators/get Functions

		function getEmailMessage() {
			return $this->emailMessage;
		}

		function getEmailSubject() {
			return $this->emailSubject;
		}

		function getRecipientAddress() {
			return $this->recipientAddress;
		}

		function getSenderAddress() {
			return $this->senderAddress;
		}	

		//Processing functions 

		function sendEmail(){

			$fromAddress = 'From:' . $this->getSenderAddress();
			mail($this->getRecipientAddress(), 
				$this->getEmailSubject(), 
				$this->getEmailMessage(), 
				$fromAddress);
		}
	}
?>









