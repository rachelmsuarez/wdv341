<!DOCTYPE html>
	<head>
		<title>PHP WDV341 Assignment 1</title>
		<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
		<meta name="viewport" content="width=device-width" initial-scale="1">
		<meta name="author" content="Rachel Suarez">
		<style>
			body {
				font-size:18px;
				font-family: 'Helvetica', arial, sans-serif;
			}
		</style>
		<?php 
			/* 
				Create a variable called yourName.  
				Assign it a value of your name.
				Display the assignment name in an h1 element on the page. 
				Include the elements in your output. 
				Use HTML to put an h2 element on the page. 
				Use PHP to display your name inside the element using the variable.
				Create the following variables:  number1, number2 and total.  
				Assign a value to them.       
				Display the value of each variable and the total variable when you add them together. 
			*/
			$yourName = "Rachel Suarez";
			$number1 = 20;
			$number2 = 5;

		?>
	</head>
	<body>	
		<div>
			<?php echo "<h1>Assignment: PHP Basics</h1>" ?>
			<h2><?php echo "Rachel Suarez" ?></h2>
			<p><?php echo $number1 ?></p>
			<p><?php echo $number2 ?></p>
			<p><?php echo $number1 + $number2 ?></p>
			<p><?php echo "<script>let langArray = ['PHP','HTML','Javascript']</script>"; ?></p>
			<p><?php echo "<script>document.write( langArray );</script>"; ?></p>
		</div>	
	</body>
</html>
